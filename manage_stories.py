"""Utilities for storing and loading stories in binary format"""
import hashlib
import os
import pickle
from typing import List

from story import Story


def save_stories(stories: List[Story], filename: str) -> None:
    """Save the array specified by stories into filename"""
    if not os.path.exists(filename):
        with open(filename, 'wb') as file:
            pickle.dump(stories, file)


def load_stories(task_name: str, dataset_type: str) -> List[Story]:
    """Load stories from filename and return them"""
    preprocess_stories('{}_stories_tokenized'.format(task_name), '{}_urls'.format(task_name), task_name)
    filename = os.path.join('data', 'raw_stories', '{}_{}_stories.pickle'.format(task_name, dataset_type))
    with open(filename, 'rb') as file:
        stories = pickle.load(file)
        return stories


def path_from_str(dirname: str) -> str:
    """Helper function"""
    dir_path = os.path.join(os.getcwd(), 'data', dirname)
    if not os.path.isdir(dir_path):
        print('{} is not a directory!'.format(dir_path))
        return ''
    return dir_path


def preprocess_stories(story_dir: str, urls_dir: str, task_name: str) -> None:
    """Save raw stories files using split from Hermann et al (2015)"""
    # import manage_stories as mns
    # mns.preprocess_stories('cnn_stories/cnn/stories', 'cnn', 'cnn')

    stories_files = set(filename for filename in os.listdir(path_from_str(story_dir)) if filename.endswith('.story'))
    file_count = 0
    total_count = len(stories_files)

    for dir_type in ['training', 'validation', 'test']:
        os.makedirs('data/raw_stories/', exist_ok=True)
        pickle_filename = os.path.join('data', 'raw_stories', '{}_{}_stories.pickle'.format(task_name, dir_type))
        if os.path.exists(pickle_filename):
            continue
        stories = []
        with open(os.path.join(path_from_str(urls_dir), 'wayback_{}_urls.txt'.format(dir_type)), 'r') as url_file:
            for url in url_file.readlines():
                hex_name = hashlib.sha1()
                hex_name.update(str(url.strip()).encode('utf-8'))
                hex_name = '{}.story'.format(hex_name.hexdigest())
                if hex_name not in stories_files:
                    print('{} not found'.format(hex_name))
                    continue
                with open(os.path.join(path_from_str(story_dir), hex_name), 'r') as story_file:
                    text, *summaries = story_file.read().split('@highlight')
                    stories.append(Story(text, summaries, hex_name, dir_type))

                    if file_count % 1000 == 0:
                        print('Processing {}/{}'.format(file_count, total_count))
                    file_count += 1

        save_stories(stories, pickle_filename)


def return_stories(task_name: str, dataset_type: str) -> List[Story]:
    """Return stories according to the dataset_type"""
    if dataset_type == 'train':
        return load_stories(task_name, 'training')
    if dataset_type == 'dev':
        return load_stories(task_name, 'validation')
    if dataset_type == 'test':
        return load_stories(task_name, 'test')
    return []
