"""Preprocessing constants and utility functions"""
import re
import string
from typing import Callable, List

import gensim.parsing.preprocessing as gpp
# from gensim.parsing.preprocessing import (
#     strip_numeric, strip_multiple_whitespaces
# )
# from gensim import utils


REMOVED_CHARS = ["''", '``', "'s", '--', '-rrb-', 'cnn', '-lrb-', '...', 'th', 'ca', '-lsb-', 'rsb',
                 'wo', 'sen.', 'dr.', 'de', 'p.m.', 'a.m.', 'no.', 'st.', 'rep.', 'sr.']
REMOVED_WORDS = set(list(string.punctuation) + REMOVED_CHARS) - {',', '$', '£', '€'}


# from https://github.com/abisee/cnn-dailymail/blob/master/make_datafiles.py
DM_S_CLOSE_QUOTE = u'\u2019'  # unicode
DM_D_CLOSE_QUOTE = u'\u201d'
END_TOKENS = ['.', '!', '?', '...', "'", "`", '"', DM_S_CLOSE_QUOTE, DM_D_CLOSE_QUOTE, ")"]
# acceptable ways to end a sentence


def fix_missing_period(line: str) -> str:
    """Adds a period to a line that is missing a period"""
    if line == "" or line[-1] in END_TOKENS:
        return line
    return "{} .".format(line)


def fix_lines_ending(s: str) -> str:
    """Apply fix_missing_period to each line"""
    lines = s.splitlines()
    lines = [fix_missing_period(line) for line in lines]
    return ' '.join(lines)


def remove_chars(s: str) -> str:
    """Lowercase input string and eliminate various tokens that are not needed"""
    s = s.lower()
    return " ".join(w for w in s.split() if w not in REMOVED_WORDS)


def expand_contractions(s: str) -> str:
    """Expand contractions to reduce vocab size"""
    # https://stackoverflow.com/questions/19790188/expanding-english-language-contractions-in-python
    contractions_dict = {
        "'cause": "because",
        "'re": "are",
        "'ve": "have",
        "gon na": "going to",
        "wan na": "want to",
        "'d": "had",
        "'d've": "would have",
        "'ll": "will",
        "he 'd": "he would",
        "he 's": "he is",
        "how 'd": "how did",
        "how 's": "how is",
        "i 'd": "i would",
        "i 'm": "i am",
        "it 'd": "it would",
        "it 's": "it is",
        "let 's": "let us",
        "n't": "not",
        "n't've": "not have",
        "o 'clock": "of the clock",
        "she 'd": "she would",
        "she 's": "she is",
        "that 'd": "that would",
        "that 's": "that is",
        "there 'd": "there would",
        "there 's": "there is",
        "they 'd": "they would",
        "to 've": "to have",
        "what 's": "what is",
        "what 've": "what have",
        "when 's": "when is",
        "when 've": "when have",
        "where 'd": "where did",
        "where 's": "where is",
        "who 's": "who is",
        "you 'd": "you would"
    }
    contractions_re = re.compile('(%s)' % '|'.join(contractions_dict.keys()))

    return contractions_re.sub(lambda x: contractions_dict[x.group(0)], s)


def normalize_numbers(s: str) -> str:
    """Convert each number to the special N token"""
    nr_re = re.compile(r'\d+\.*\d*')
    return nr_re.sub('N', s)


def preprocessing_filters() -> List[Callable[[str], str]]:
    """Wrapper for preprocessing filters"""

    return [fix_lines_ending, remove_chars, expand_contractions,
            normalize_numbers, gpp.strip_multiple_whitespaces]


def text_filters() -> List[Callable[[str], str]]:
    """Filters to be applied on source text"""
    return preprocessing_filters() + [gpp.remove_stopwords]


def summary_filters() -> List[Callable[[str], str]]:
    """Filters to be applied on target text"""
    return preprocessing_filters()
