"""A Story encapsulates a text with its' summaries"""
from typing import Any, Dict, Iterator, List


class BaseStory:
    """Example class used to wrap text and summaries"""
    def __init__(self, text: Any, summaries: Iterator[Any]):
        self.text = text
        self.summaries = summaries

    def __str__(self) -> str:
        return 'Text: {}\nSummaries: {}'.format(self.text, ''.join(self.summaries))

    def get_pairs(self) -> List[Dict[str, str]]:
        """Pairs for training"""

        pairs = []
        for summary in self.summaries:
            pairs.append({'text': self.text, 'summary': summary})
        return pairs


class Story(BaseStory):
    """A Story object contains a text string and an array of summaries"""

    def __init__(self, text: str, summaries: List[str], filename: str, dir_type: str):
        super().__init__(text, summaries)
        self.filename = filename
        self.dir_type = dir_type

    def full_text(self) -> str:
        """Full text of Story required for computing embeddings"""

        return self.text.join(self.summaries)
