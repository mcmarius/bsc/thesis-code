"""Constants, hyper/parameters"""
import torch


device = torch.device('cuda')

EMPTY = 0
UNK = 1
SENT_START = 2
SENT_END = 3
SOS = 4
EOS = 5
START_IDX = EOS + 1

SENT_START_TOKEN = '<S>'
SENT_END_TOKEN = '</S>'

EMBEDDING_SIZE = 64
HIDDEN_SIZE = 32
N_LAYERS = 2

CUT_DATASET = True
MAX_EXAMPLES = 32768*2
SEQ_MIN_LENGTH = 0
SEQ_MAX_LENGTH = 63  # 2^N - 1 because of <EOS>
MAX_SUMMARY_LENGTH = 32
CLAMP_SOURCE_TEXT = True
CLAMP_TARGET_TEXT = True
ADA_SOFTMAX = True
USE_ATTN = True

P_DROPOUT = 0.4

# BEAM_SEARCH = True
# BEAM_SIZE = 5
# todo check freq order in fields dict

# max examples (31 max len)    128  |  256  |  512  | 1_024 | 2_048 | 4_096 | 8_192 |  16k  | 32768 | 65536 |
# VOCAB_SIZE = 32768        # //256 | //128 | //64  | //32  | //16  | //8   | //4   |  //2  |   1   |  *2   | *4
SOURCE_VOCAB_SIZE = 65536   # 10090 | 14434 | 20850 | 29519 | 41561 | 58522 | ..... | ..... | ..... | ..... | .....
TARGET_VOCAB_SIZE = 32768   # 2092  | 3433  | 5380  | 8203  | 12153 | 17694 | ..... | ..... | ..... | ..... | .....
BATCH_SIZE = 512
# TEACHER_FORCING_RATIO = 0.2  # bigger means use more teacher forcing
# TF_1 = 150
# TF_R1 = 0.9
# TF_2 = 200
# TF_R2 = 0.5
# TF_E = 0.1
LEARNING_RATE = 1e-2
EPOCHS = 31

TASK_NAME = 'cnn'

EVAL_TYPE = 'val'

TABLES_DIR = '../doc/tables/'

PRINT_INTERVAL = 1
EVAL_INTERVAL = 3
SAVE_INTERVAL = 3
