import math
from typing import Any, List, Tuple

import torch
import torch.nn as nn

import constants as const

device = const.device
torch.set_default_tensor_type('torch.FloatTensor')


class EncoderRNN(nn.Module):
    def __init__(self, input_size: int, embedding_size: int, hidden_size: int):
        super().__init__()
        self.hidden_size = hidden_size
        self.embedding_size = embedding_size
        self.embedding = nn.Embedding(input_size, embedding_size)
        self.dropout = nn.Dropout(const.P_DROPOUT)
        self.gru = nn.GRU(embedding_size, hidden_size,
                          num_layers=const.N_LAYERS, dropout=const.P_DROPOUT,
                          bidirectional=True, batch_first=True)

    def forward(self, nn_input: torch.Tensor, lengths: List[torch.Tensor], hidden: torch.Tensor = None) \
            -> Tuple[torch.Tensor, torch.Tensor]:
        # https://github.com/AuCson/PyTorch-Batch-Attention-Seq2seq/blob/master/attentionRNN.py
        embedded = self.embedding(nn_input)
        embedded = self.dropout(embedded)
        packed = torch.nn.utils.rnn.pack_padded_sequence(embedded, lengths, batch_first=True)
        outputs, hidden = self.gru(packed, hidden)
        # unpack (back to padded)
        outputs, output_lengths = torch.nn.utils.rnn.pad_packed_sequence(outputs, padding_value=1, batch_first=True)
        # outputs = outputs.view(-1, const.BATCH_SIZE, 2, self.hidden_size)
        # outputs = outputs[:, :, 0, :] + outputs[:, :, 1, :]  # Sum bidirectional outputs
        outputs = outputs[:, :, :self.hidden_size] + outputs[:, :, self.hidden_size:]  # Sum bidirectional outputs
        # num_directions = 2 if self.gru.bidirectional else 1
        # -1 is sequence max length
        # outputs = outputs.contiguous().view(const.BATCH_SIZE, -1, num_directions, self.hidden_size)
        return outputs, hidden

    def init_hidden(self) \
            -> torch.Tensor:
        # num_directions, batch_size, hidden_size
        if self.gru.bidirectional:
            return torch.zeros(2 * const.N_LAYERS, const.BATCH_SIZE, self.hidden_size, device=device)
        return torch.zeros(const.N_LAYERS, const.BATCH_SIZE, self.hidden_size, device=device)


class DecoderRNN(nn.Module):
    def __init__(self, embedding_size: int, hidden_size: int, output_size: int):
        super().__init__()
        self.embedding_size = embedding_size
        self.hidden_size = hidden_size
        self.output_size = output_size

        self.embedding = nn.Embedding(output_size, embedding_size)
        self.gru = nn.GRU(embedding_size, hidden_size,
                          num_layers=const.N_LAYERS, dropout=const.P_DROPOUT, batch_first=True)
        self.dropout = nn.Dropout(const.P_DROPOUT)
        if const.ADA_SOFTMAX:
            vs = output_size  # vocabulary size
            self.out = torch.nn.AdaptiveLogSoftmaxWithLoss(in_features=hidden_size,
                                                           n_classes=vs,
                                                           cutoffs=[1024])
        else:
            self.out = nn.Linear(hidden_size, output_size)

    def forward(self, nn_input: torch.Tensor, hidden: torch.Tensor, target=None) \
            -> Tuple[torch.Tensor, torch.Tensor, Any]:
        output = self.embedding(nn_input).view(const.BATCH_SIZE, 1, -1)
        hidden = hidden.view(const.N_LAYERS, const.BATCH_SIZE, self.hidden_size)
        output = nn.functional.relu(output)
        output, hidden = self.gru(output, hidden)
        output = self.dropout(output)
        hidden = hidden.view(const.BATCH_SIZE, const.N_LAYERS, self.hidden_size)
        loss = None
        if const.ADA_SOFTMAX:
            if target is not None:
                _, loss = self.out(input=output.view(const.BATCH_SIZE, -1), target=target)
            output = self.out.predict(output.view(const.BATCH_SIZE, -1))
        else:
            output = nn.functional.log_softmax(self.out(output), dim=2)
        return output, hidden, loss


class Attention(nn.Module):

    def __init__(self, hidden_size: int):
        super().__init__()
        self.hidden_size = hidden_size
        self.attn = nn.Linear(hidden_size * 2, hidden_size)
        self.w = nn.Parameter(torch.rand(hidden_size))
        # normalize weights
        std_dev = 1. / math.sqrt(self.w.size(0))
        self.w.data.normal_(mean=0, std=std_dev)

    def forward(self, hidden, encoder_outputs):
        max_length = encoder_outputs.size(1)
        h = hidden.repeat(max_length, 1, 1).transpose(0, 1)
        attns = self.score(h, encoder_outputs)
        return nn.functional.softmax(attns, dim=1)

    def score(self, hidden, encoder_outputs):
        signal = torch.tanh(self.attn(torch.cat([hidden, encoder_outputs], dim=2)))
        # repeat batch size
        w = self.w.repeat(encoder_outputs.size(0), 1).unsqueeze(1)
        return torch.bmm(w, signal.transpose(1, 2)).squeeze(1)


class AttnDecoderRNN(nn.Module):
    def __init__(self, embedding_size: int, hidden_size: int, output_size: int, dropout_p: float = const.P_DROPOUT,
                 max_length: int = const.SEQ_MAX_LENGTH):
        super().__init__()
        self.embedding_size = embedding_size
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.dropout_p = dropout_p
        self.max_length = max_length

        self.embedding = nn.Embedding(output_size, embedding_size)
        self.attn = Attention(hidden_size)
        self.dropout = nn.Dropout(dropout_p)
        self.gru = nn.GRU(embedding_size + hidden_size, hidden_size,
                          num_layers=const.N_LAYERS, dropout=const.P_DROPOUT, batch_first=True)
        # self.linear = nn.Linear(2 * hidden_size, hidden_size)

        if const.ADA_SOFTMAX:
            vs = output_size  # vocabulary size
            self.out = torch.nn.AdaptiveLogSoftmaxWithLoss(in_features=hidden_size,
                                                           n_classes=vs,
                                                           cutoffs=[1024])
        else:
            self.out = nn.Linear(hidden_size, output_size)

    def forward(self, nn_input, hidden, encoder_outputs, target=None):
        embedded = self.embedding(nn_input).view(const.BATCH_SIZE, 1, -1)
        embedded = self.dropout(embedded)
        # _output, hidden = self.gru(embedded, hidden)
        # https://stackoverflow.com/questions/48302810/whats-the-difference-between-hidden-and-output-in-pytorch-lstm
        # take only the hidden state of the last layer; transpose to be batch first
        # attn_weights = nn.functional.softmax(self.attn(torch.bmm(encoder_outputs.transpose(2, 1), h)), dim=1)
        # hidden[-1]: (batch_size, hidden_size)
        # encoder_outputs: (batch_size, seq_max_length, hidden_size)
        attn_weights = self.attn(hidden[-1], encoder_outputs)  # (batch_size, seq_max_length)
        context = attn_weights.unsqueeze(1).bmm(encoder_outputs)  # (batch_size, 1, hidden_size)

        output, hidden = self.gru(torch.cat((embedded, context), dim=2), hidden)
        loss = None
        if const.ADA_SOFTMAX:
            if target is not None:
                _, loss = self.out(input=output.view(const.BATCH_SIZE, -1), target=target)
            output = self.out.predict(output.view(const.BATCH_SIZE, -1))
        else:
            output = nn.functional.log_softmax(self.out(output), dim=2)
        return output, hidden, attn_weights, loss


def get_model() \
        -> Tuple[nn.Module, nn.Module]:
    # encoder = EncoderRNN(w2v_model=w2v_model)
    # decoder = AttnDecoderRNN(w2v_model=w2v_model)
    encoder = EncoderRNN(const.SOURCE_VOCAB_SIZE, const.EMBEDDING_SIZE, const.HIDDEN_SIZE)
    if const.USE_ATTN:
        decoder = AttnDecoderRNN(const.EMBEDDING_SIZE, const.HIDDEN_SIZE, const.TARGET_VOCAB_SIZE)
    else:
        decoder = DecoderRNN(const.EMBEDDING_SIZE, const.HIDDEN_SIZE, const.TARGET_VOCAB_SIZE)
    return encoder, decoder
