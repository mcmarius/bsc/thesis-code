import torch
from collections import OrderedDict
from typing import List, Tuple

from torchtext.data.field import ReversibleField
import torchtext.data as data

import constants as const
import preprocessing_stories as ps


def clamp_text(text: List[str], max_length: int = const.SEQ_MAX_LENGTH) -> List[str]:
    """Trim input according to max_length"""
    if not const.CLAMP_SOURCE_TEXT:
        return text
    n = len(text)
    return text[0:min(n, max_length)-1]  # -1 because of '<EOS>'


class SummarizationDataset(data.Dataset):
    def __init__(self, stories, fields, filter_pred=None):
        if not isinstance(fields[0], (tuple, list)):
            fields = [('src', fields[0]), ('trg', fields[1])]
        examples = [data.Example.fromlist([clamp_text(story.text),
                                           clamp_text(story.summaries[0] + story.summaries[1]
                                                      if len(story.summaries) > 1
                                                      else story.summaries[0])
                                           ], fields)
                    for story in stories]

        super().__init__(examples, fields, filter_pred)

    @classmethod
    def default_splits(cls, task_name: str, fields, train=None, val=None, test=None) \
            -> Tuple['SummarizationDataset', 'SummarizationDataset', 'SummarizationDataset']:
        if train:
            _, train_stories = ps.load_dataset(task_name, 'train')
            if const.CUT_DATASET:
                train_stories = train_stories[:const.MAX_EXAMPLES]
            train_data = cls(train_stories, fields)
        else:
            train_data = None
        if val:
            _, val_stories = ps.load_dataset(task_name, 'dev')
            # if const.CUT_DATASET:
            #    val_stories = val_stories[:min(const.BATCH_SIZE, 1024)]
            val_data = cls(val_stories, fields)
        else:
            val_data = None
        if test:
            _, test_stories = ps.load_dataset(task_name, 'test')
            test_data = cls(test_stories, fields)
        else:
            test_data = None
        return train_data, val_data, test_data


class CustomField(ReversibleField):
    def __init__(self, sequential=True, use_vocab=True, init_token="<SOS>",
                 eos_token="<EOS>", fix_length=None, dtype=torch.long,
                 preprocessing=None, postprocessing=None, lower=False,
                 tokenize=(lambda s: s), include_lengths=False,
                 batch_first=False, pad_token="<pad>", unk_token="<unk>",
                 pad_first=False, truncate_first=False, stop_words=None,
                 is_target=False):
        super().__init__(sequential=sequential, use_vocab=use_vocab, init_token=init_token, eos_token=eos_token,
                         fix_length=fix_length, dtype=dtype, preprocessing=preprocessing,
                         postprocessing=postprocessing, lower=lower, tokenize=tokenize,
                         include_lengths=include_lengths, batch_first=batch_first, pad_token=pad_token,
                         unk_token=unk_token, pad_first=pad_first, truncate_first=truncate_first,
                         stop_words=stop_words, is_target=is_target)
        self.vocab = None

    def build_vocab(self, counter, **kwargs):
        specials = list(OrderedDict.fromkeys(
            tok for tok in [self.unk_token, self.pad_token, self.init_token,
                            self.eos_token]
            if tok is not None))
        self.vocab = self.vocab_cls(counter, specials=specials, **kwargs)

    def reverse(self, batch):
        if self.use_revtok:
            try:
                import revtok
            except ImportError:
                print("Please install revtok.")
                raise
        if not self.batch_first:
            batch = batch.t()
        with torch.cuda.device_of(batch):
            batch = batch.tolist()
        batch = [[self.vocab.itos[ind] for ind in ex] for ex in batch]  # denumericalize

        def trim(s, t):
            sentence = []
            for w in s:
                if w == t:
                    break
                sentence.append(w)
            return sentence

        batch = [trim(ex, self.eos_token) for ex in batch]  # trim past frst eos

        def filter_special(tok):
            return tok not in (self.init_token, self.pad_token)

        batch = [filter(filter_special, ex) for ex in batch]
        # if self.use_revtok:
        #     return [revtok.detokenize(ex) for ex in batch]
        return [' '.join(ex) for ex in batch]


class BatchWrapper:
    def __init__(self, data_it, x_name, y_name) -> None:
        self.data_it, self.x_name, self.y_name = data_it, x_name, y_name

    def __iter__(self):
        for i, batch in enumerate(self.data_it):
            x = getattr(batch, self.x_name)
            y = getattr(batch, self.y_name)
            yield i, x, y

    def __len__(self):
        return len(self.data_it)


"""
import prepare_data
import manage_vocab as mnv
import torchtext
task_name, dataset_type = 'cnn', 'dev'
freq, _, _ = mnv.return_vocab(task_name, dataset_type)
# _, texts_freq, summaries_freq = mnv.return_vocab(task_name, dataset_type)
t_field = prepare_data.CustomField()
s_field = prepare_data.CustomField(is_target=True)
t_field.build_vocab(texts_freq)
s_field.build_vocab(summaries_freq)
# using same vocab for both source and target
# or initialize 2 CustomFields
train_ds, val_ds, _ = prepare_data.SummarizationDataset.default_splits(task_name, [t_field, s_field], train=1, val=1)
train_iter, val_iter = torchtext.data.BucketIterator.splits((train_ds, val_ds),
 batch_size=const.BATCH_SIZE, sort_key=lambda x: len(x.src))

"""
