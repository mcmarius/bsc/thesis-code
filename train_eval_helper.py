"""Common functions for train and eval"""
import random
from datetime import datetime
from typing import List, Tuple

import tensorboardX
import torch
import torch.nn as nn
import torchtext
from rouge import Rouge

import constants as const
import manage_vocab as mnv
import prepare_data

device = const.device


def experiment_name(ds_type: str):
    current_time = datetime.now().strftime('%b%d_%H-%M-%S')
    attention = 1 if const.USE_ATTN else 0
    adaptive_softmax = 1 if const.ADA_SOFTMAX else 0
    name = '{}_e{}_h{}_nl{}_ex{}_at{}_as{}_bs{}_sv{}_tv{}_len{}_d{}_tn{}_dt-{}' \
        .format(current_time, const.EMBEDDING_SIZE, const.HIDDEN_SIZE, const.N_LAYERS, const.MAX_EXAMPLES,
                attention, adaptive_softmax, const.BATCH_SIZE,
                const.SOURCE_VOCAB_SIZE, const.TARGET_VOCAB_SIZE,
                const.SEQ_MAX_LENGTH, const.P_DROPOUT,
                # const.LEARNING_RATE, # optimizer class, cell type
                const.TASK_NAME, ds_type)
    return name


def save_state(encoder: nn.Module, decoder: nn.Module,
               encoder_optimizer: torch.optim.Optimizer, decoder_optimizer: torch.optim.Optimizer,
               epoch: int, loss: float, task_name: str) \
        -> None:
    print('\nSaving checkpoint for epoch #{}.\n'.format(epoch))
    torch.save({
        'encoder': encoder.state_dict(),
        'decoder': decoder.state_dict(),
        'encoder_optimizer': encoder_optimizer.state_dict(),
        'decoder_optimizer': decoder_optimizer.state_dict(),
        'loss': loss
    }, 'data/checkpoint_{}_{}_{}_{}_{}_{}_{}_{}.pt'.format(task_name, epoch,
                                                           const.BATCH_SIZE,  # dropout?
                                                           const.SOURCE_VOCAB_SIZE,  # different vocabs?
                                                           const.TARGET_VOCAB_SIZE,
                                                           const.EMBEDDING_SIZE, const.HIDDEN_SIZE,
                                                           const.SEQ_MAX_LENGTH))


def load_state(encoder: nn.Module, decoder: nn.Module,
               encoder_optimizer: torch.optim.Optimizer, decoder_optimizer: torch.optim.Optimizer,
               epoch: int, task_name: str) \
        -> Tuple[nn.Module, nn.Module, torch.optim.Optimizer, torch.optim.Optimizer, int, float]:
    checkpoint = torch.load('data/checkpoint_{}_{}_{}_{}_{}_{}_{}_{}.pt'.format(task_name, epoch,
                                                                                const.BATCH_SIZE,
                                                                                const.SOURCE_VOCAB_SIZE,
                                                                                const.TARGET_VOCAB_SIZE,
                                                                                const.EMBEDDING_SIZE, const.HIDDEN_SIZE,
                                                                                const.SEQ_MAX_LENGTH))
    encoder.load_state_dict(checkpoint['encoder'])
    encoder.to(device)
    decoder.load_state_dict(checkpoint['decoder'])
    decoder.to(device)
    encoder_optimizer.load_state_dict(checkpoint['encoder_optimizer'])
    # encoder_optimizer.to(device)
    decoder_optimizer.load_state_dict(checkpoint['decoder_optimizer'])
    # decoder_optimizer.to(device)
    loss = checkpoint['loss']
    return encoder, decoder, encoder_optimizer, decoder_optimizer, epoch, loss


def load_dataset_wrappers(task_name: str) \
        -> Tuple[prepare_data.SummarizationDataset, prepare_data.SummarizationDataset,
                 prepare_data.SummarizationDataset]:
    # freq, _, _ = mnv.return_vocab(task_name, dataset_type)
    _, texts_freq, summaries_freq = mnv.return_vocab(task_name, 'train')
    t_field = prepare_data.CustomField()
    s_field = prepare_data.CustomField(is_target=True)

    t_field.build_vocab(texts_freq, max_size=const.SOURCE_VOCAB_SIZE - 4)
    s_field.build_vocab(summaries_freq, max_size=const.TARGET_VOCAB_SIZE - 4)
    # using same vocab for both source and target
    # or initialize 2 CustomFields
    # t_field.build_vocab(freq, max_size=const.VOCAB_SIZE - 4)
    # s_field.build_vocab(freq, max_size=const.VOCAB_SIZE - 4)
    t_ds, v_ds, test_ds = prepare_data.SummarizationDataset.default_splits(task_name,
                                                                           [t_field, s_field], train=1, val=1,
                                                                           test=1)
    return t_ds, v_ds, test_ds


def get_iterators(train_ds: prepare_data.SummarizationDataset,
                  val_ds: prepare_data.SummarizationDataset,
                  test_ds: prepare_data.SummarizationDataset = None) \
        -> Tuple[prepare_data.BatchWrapper, prepare_data.BatchWrapper, prepare_data.BatchWrapper]:
    train_iter, val_iter, test_iter = torchtext.data.BucketIterator.splits((train_ds, val_ds, test_ds),
                                                                           sort_key=lambda x: len(x.src),
                                                                           batch_size=const.BATCH_SIZE,
                                                                           repeat=False,
                                                                           shuffle=True, device=device)
    train_it = prepare_data.BatchWrapper(train_iter, 'src', 'trg')
    val_it = prepare_data.BatchWrapper(val_iter, 'src', 'trg')
    test_it = prepare_data.BatchWrapper(test_iter, 'src', 'trg')
    return train_it, val_it, test_it


def tensor_from_sentence(sentence: List[int]) \
        -> torch.Tensor:
    return torch.tensor(sentence, device=device).view(-1, 1)


def evaluate(encoder: nn.Module, decoder: nn.Module, field,
             input_tensor, target_tensor, loss_criterion):
    encoder = encoder.eval()
    decoder = decoder.eval()
    with torch.no_grad():
        # input_tensor = tensor_from_sentence(sentence)
        # input_length = input_tensor.size()[0]
        encoder_hidden = None  # encoder.init_hidden()

        # encoder_outputs = torch.zeros(max_length, encoder.hidden_size, device=device)
        input_tensor, lengths, _ = get_sequence_lengths(input_tensor)
        # for i in range(input_length):
        encoder_outputs, encoder_hidden = encoder(input_tensor, lengths, encoder_hidden)
        # encoder_outputs[i] = encoder_output[0, 0]

        enc_hidden = encoder_hidden.view(const.N_LAYERS, 2, const.BATCH_SIZE, -1)
        decoder_hidden = enc_hidden[:, 0, :, :] + enc_hidden[:, 1, :, :]

        decoder_input = target_tensor[0]
        decoded_words = field.reverse(decoder_input.view(1, -1))
        decoded_words = [[word] for word in decoded_words]
        decoder_attentions = torch.zeros(const.MAX_SUMMARY_LENGTH, const.BATCH_SIZE, const.SEQ_MAX_LENGTH + 1)

        loss = 0
        tensor_length = target_tensor.size(0)

        for i in range(1, const.MAX_SUMMARY_LENGTH):
            if const.USE_ATTN:
                decoder_output, decoder_hidden, attn_weights, loss_ = decoder(decoder_input, decoder_hidden,
                                                                              encoder_outputs,
                                                                              target_tensor[min(i, tensor_length) - 1])
                decoder_attentions[i] = attn_weights.data
            else:
                decoder_output, decoder_hidden, loss_ = decoder(decoder_input, decoder_hidden,
                                                                target_tensor[min(i, tensor_length) - 1])
            if not const.ADA_SOFTMAX:
                _, top_i = decoder_output.topk(1, dim=2)
                loss_ = loss_criterion(decoder_output.view(const.BATCH_SIZE, const.TARGET_VOCAB_SIZE),
                                       target_tensor[min(i, tensor_length) - 1])
            loss += loss_

            if const.ADA_SOFTMAX:
                rez = field.reverse(decoder_output.view(1, -1))
                decoder_input = decoder_output
            else:
                _, top_i = decoder_output.data.topk(1, dim=2)
                rez = field.reverse(top_i[:, 0].view(1, -1))
                decoder_input = top_i.squeeze().detach()
            decoded_words = [(*a, b) for a, b in zip(decoded_words, rez)]
        return decoded_words, loss.item() / tensor_length, decoder_attentions


def evaluate_randomly(encoder: nn.Module, decoder: nn.Module,
                      train_ds: prepare_data.SummarizationDataset,
                      iterator: prepare_data.BatchWrapper, loss_criterion,
                      n, logger: tensorboardX.SummaryWriter) \
        -> int:
    rouge = Rouge()
    source_field = train_ds.fields['src']
    target_field = train_ds.fields['trg']
    expected_outputs = []
    actual_outputs = []
    text = []
    losses = 0
    for i, input_tensor, target_tensor in iterator:
        if input_tensor.size(1) != const.BATCH_SIZE:
            continue
        output_words, loss, _ = evaluate(encoder, decoder, target_field, input_tensor, target_tensor, loss_criterion)
        logger.add_scalar('individual_val_loss', loss, i + n * len(iterator))
        decoded_targets = target_field.reverse(target_tensor)
        # plt.matshow(attentions.numpy())
        # plt.show()
        [actual_outputs.append(' '.join(sent_words)) for sent_words in output_words]
        [expected_outputs.append(sent) for sent in decoded_targets]
        [text.append(t) for t in source_field.reverse(input_tensor)]
        losses += loss

    random_example = random.randint(0, const.BATCH_SIZE - 1)
    print('Expect: {}\nGot: {}\nText: {}'.format(expected_outputs[random_example],
                                                 actual_outputs[random_example],
                                                 ' '.join(prepare_data.clamp_text(text[random_example].split()))))
    scores_dict = rouge.get_scores(actual_outputs, expected_outputs, avg=True)
    print(scores_dict)
    logger.add_scalars('rouge-1-score', scores_dict['rouge-1'], n)
    logger.add_scalars('rouge-2-score', scores_dict['rouge-2'], n)
    logger.add_scalars('rouge-L-score', scores_dict['rouge-l'], n)
    logger.add_text('actual_output', actual_outputs[random_example], n)
    logger.add_text('trg_output', expected_outputs[random_example], n)
    logger.add_text('src_input', ' '.join(prepare_data.clamp_text(text[random_example].split())), n)
    return losses


def get_sequence_lengths(input_tensor, target_tensor: torch.Tensor = None) \
        -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
    def my_len(arrays):
        return [len([x for x in array if x.item() != 1]) for array in arrays]

    i_tns = input_tensor.permute(1, 0)
    lengths = torch.LongTensor([my_len(i_tns)]).view(-1, 1)
    if target_tensor is not None:
        t_tns = target_tensor.permute(1, 0)
        zipped = list(zip(i_tns, lengths, t_tns))
        input_tensor, lengths, target_tensor = zip(*sorted(zipped, key=lambda y: y[1], reverse=True))
    else:
        zipped = list(zip(i_tns, lengths))
        input_tensor, lengths = zip(*sorted(zipped, key=lambda y: y[1], reverse=True))
    lengths = torch.cat(lengths, dim=0)
    input_tensor = torch.cat(input_tensor, dim=0).view(const.BATCH_SIZE, -1)
    if target_tensor is not None:
        target_tensor = torch.cat(target_tensor, dim=0).view(const.BATCH_SIZE, -1)
    return input_tensor, lengths, target_tensor
