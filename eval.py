"""Evaluate model on dev/test dataset"""
import os
from sys import argv

import matplotlib.pyplot as plt
import numpy as np
import tensorboardX
import torch
import torch.nn as nn
from rouge import Rouge

import constants as const
import models
import prepare_data
import train_eval_helper as teh


device = const.device
torch.set_default_tensor_type('torch.FloatTensor')
if const.EVAL_TYPE == 'val':
    ds_type = 'v'
else:
    ds_type = 'e'
logger = tensorboardX.SummaryWriter(log_dir=os.path.join('runs', teh.experiment_name(ds_type)))


def evaluate_dataset(encoder: nn.Module, decoder: nn.Module,
                     train_ds: prepare_data.SummarizationDataset,
                     iterator: prepare_data.BatchWrapper, loss_criterion):
    compute_rouge = False
    rouge = Rouge()
    source_field = train_ds.fields['src']
    target_field = train_ds.fields['trg']
    expected_outputs = []
    actual_outputs = []
    text = []
    # losses = 0
    for i, input_tensor, target_tensor in iterator:
        if input_tensor.size(1) != const.BATCH_SIZE:
            continue
        output_words, _, attentions = teh.evaluate(encoder, decoder, target_field, input_tensor, target_tensor,
                                                   loss_criterion)
        # logger.add_scalar('individual_val_loss', loss, i)
        decoded_targets = target_field.reverse(target_tensor)
        # plt.matshow(attentions.numpy())

        # plt.show()
        [actual_outputs.append(' '.join(sent_words)) for sent_words in output_words]
        [expected_outputs.append(sent) for sent in decoded_targets]
        [text.append(t) for t in source_field.reverse(input_tensor)]
        # losses += loss

        for j in range(const.BATCH_SIZE):
            fig = plt.figure()
            ax = fig.add_subplot(111)
            ax.matshow(attentions[:, j].numpy())
            ax.set_xticks(np.arange(len(text[j].split())))
            ax.set_yticks(np.arange(len(actual_outputs[j].split())))
            ax.set_xticklabels(text[j].split(), rotation='vertical')
            ax.set_yticklabels(actual_outputs[j].split())
            logger.add_figure('attention_weights', fig, i*const.BATCH_SIZE + j)

            logger.add_text('actual_output', actual_outputs[j], i*const.BATCH_SIZE + j)
            logger.add_text('trg_output', expected_outputs[j], i*const.BATCH_SIZE + j)
            logger.add_text('src_input', ' '.join(prepare_data.clamp_text(text[j].split())), i*const.BATCH_SIZE + j)

    # random_example = random.randint(0, const.BATCH_SIZE - 1)
    # print('Expect: {}\nGot: {}\nText: {}'.format(expected_outputs[random_example],
    #                                              actual_outputs[random_example],
    #                                              ' '.join(prepare_data.clamp_text(text[random_example].split()))))
    if compute_rouge:
        scores_dict = rouge.get_scores(actual_outputs, expected_outputs, avg=True)
        logger.add_scalars('rouge-1-score', scores_dict['rouge-1'], 0)
        logger.add_scalars('rouge-2-score', scores_dict['rouge-2'], 0)
        logger.add_scalars('rouge-L-score', scores_dict['rouge-l'], 0)


def run():
    task_name = const.TASK_NAME
    train_ds, val_ds, test_ds = teh.load_dataset_wrappers(task_name)
    print('Finished loading data')

    encoder, decoder = models.get_model()
    encoder_optimizer = torch.optim.RMSprop(encoder.parameters(), lr=const.LEARNING_RATE, centered=True)
    decoder_optimizer = torch.optim.RMSprop(decoder.parameters(), lr=const.LEARNING_RATE, centered=True)

    loss_criterion = nn.NLLLoss(ignore_index=1)

    train_it, val_it, test_it = teh.get_iterators(train_ds, val_ds, test_ds)

    if len(argv) > 2:
        if argv[1] == "resume":
            encoder, decoder, encoder_optimizer, decoder_optimizer, start_epoch, loss \
                = teh.load_state(encoder, decoder, encoder_optimizer, decoder_optimizer, int(argv[2]), task_name)
    if const.EVAL_TYPE == 'val':
        eval_it = val_it
    else:
        eval_it = test_it
    evaluate_dataset(encoder, decoder, train_ds, eval_it, loss_criterion)


if __name__ == '__main__':
    run()
    logger.close()
