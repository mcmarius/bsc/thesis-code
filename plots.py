"""Plot module"""
import itertools
import os
import pickle
from typing import Any, Callable, List, Tuple

import matplotlib.pyplot as plt
import numpy as np

import constants as const
import manage_stories as mns
import preprocessing_stories as ps
from story import Story


plt.rc('text', usetex=True)
plt.rc('font', family='serif', size=19)


def clamp_text(text: List[str], max_length: int = const.SEQ_MAX_LENGTH) -> List[str]:
    """Trim input according to max_length"""
    if not const.CLAMP_SOURCE_TEXT:
        return text
    n = len(text)
    return text[0:min(n, max_length) - 1]  # -1 because of '<EOS>'


def plot_hist_log(lst: list, name: str='default_hist', xlabel: str='', ylabel: str='', **kwargs):
    """Plot histogram using log scale"""
    plt.yscale('log')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    multiple_plots = kwargs.pop('multiple', False)
    plt.hist(lst, **kwargs)
    plt.savefig('../doc/plots/cnn_{}.pdf'.format(name), dpi=1000, bbox_inches='tight')
    if not multiple_plots:
        plt.show()


def plot_bar_log(lst: list, name: str='default_bar'):
    """Plot bar chart using log scale"""
    labels, counts = np.unique(lst, return_counts=True)
    plt.yscale('log')
    plt.bar(labels, counts, width=0.5, align='center')
    plt.savefig('../doc/plots/cnn_{}.pdf'.format(name), dpi=1000, bbox_inches='tight')
    plt.show()


def basic_lst(func: Callable, task_name: str, dataset_type: str):
    """Transform stories list according to func"""
    t_stories = mns.return_stories(task_name, dataset_type)
    return [func(x) for x in t_stories]


def tok_lst(func: Callable, task_name: str, dataset_type: str):
    """Transform tokenized source text according to func"""
    t_stories, _ = ps.load_dataset(task_name, dataset_type)
    return [func((list(itertools.chain.from_iterable(story.text)))) for story in t_stories]


def tok_lst2(func: Callable, task_name: str, dataset_type: str):
    """Transform tokenized target text according to func"""
    t_stories, _ = ps.load_dataset(task_name, dataset_type)
    return [func(list(itertools.chain.from_iterable(story.summaries[0:2]))) for story in t_stories]


def tok_summary(func: Callable, task_name: str, dataset_type: str):
    """Transform tokenized target texts list according to func"""
    t_stories, _ = ps.load_dataset(task_name, dataset_type)
    return [func(story) for story in t_stories]


def plot_stories_stats(func: Callable, plot_func: Callable, lst: Callable,
                       task_name: str, dataset_type: str, name: str, **kwargs) -> None:
    """Generic function to plot stats"""
    plot_func(lst(func, task_name, dataset_type), name, **kwargs)


def plot_summaries_count(name: str, **kwargs) -> None:
    """Specific plot"""
    plot_stories_stats(lambda x: len(x.summaries), plot_bar_log, basic_lst, 'cnn', 'train', name, **kwargs)


def plot_summaries_length(name: str, **kwargs) -> None:
    """Plot summaries average length"""
    def func(x):
        lst = [len(y) for y in x.summaries]
        return sum(lst)/len(lst)
    plot_stories_stats(func, plot_hist_log, tok_summary, 'cnn', 'train', name, **kwargs)


def plot_summaries_one_length(name: str, **kwargs) -> None:
    """Plot summaries word length"""
    plot_stories_stats(len, plot_hist_log, tok_lst2, 'cnn', 'train', name, **kwargs)


def plot_text_length(name: str, **kwargs) -> None:
    """Plot texts word length"""
    plot_stories_stats(len, plot_hist_log, tok_lst, 'cnn', 'train', name, **kwargs)


def vocab_lst(_func: Callable, task_name: str, dataset_type: str, max_e: str, seq_max_len: str):
    """Vocabulary list"""
    file_name = os.path.join('data', 'vocab', '{}_{}_vocab_{}_{}.pickle'.format(task_name, dataset_type, max_e, seq_max_len))
    with open(file_name, 'rb') as file:
        frequencies_tuple = pickle.load(file)
    return (frequencies.values() for frequencies in frequencies_tuple)


def plot_vocab_frequency(max_e: str, seq_max_len: str, **kwargs):
    """Plot vocabulary counts"""
    a_fq, t_fq, s_fq = vocab_lst(lambda x: x, 'cnn', 'train', max_e, seq_max_len)
    kwargs.update({'multiple': True})
    # plot_hist_log(a_fq, 'a_fq', **kwargs)
    # plt.figure()
    plot_hist_log(t_fq, 't_fq_{}_{}'.format(max_e, seq_max_len), 'Frecvența cuvântului', 'Numărul de cuvinte', **kwargs)
    plt.figure()
    plot_hist_log(s_fq, 's_fq_{}_{}'.format(max_e, seq_max_len), 'Frecvența cuvântului', 'Numărul de cuvinte', **kwargs)
    plt.show()


def text_stats(lst: List[List[str]]) -> Tuple[int, int]:
    """Counts for given list of sentences"""
    sent_count = len(lst)
    word_count = len(list(itertools.chain.from_iterable(lst)))
    return sent_count, word_count


def story_stats(story: Story) -> Tuple[int, int, Any]:
    """Tuple of counts for stories and summaries"""
    story = ps.preprocess_story(story)
    t_sc, t_wc = text_stats(story.text)
    s_sc = [text_stats([summary]) for summary in story.summaries]
    s_sc = tuple(sum(z) // len(z) for z in zip(*s_sc))
    return t_sc, t_wc, (*s_sc)


def stories_stats(stories: List[Story]) -> List[Tuple[int, int, Any]]:
    """List of tuples of stats for a list of stories"""
    return [story_stats(story) for story in stories]

# import manage_stories as mns
# sto=mns.return_stories('cnn', 'dev')
# from importlib import reload
# import plots
# import gensim.parsing.preprocessing as gpp
# import preprocessing as pre
# import re
# import itertools
# results = plots.stories_stats(sto)
# scs = [x[1] for x in results]
# plots.plot_hist_log(scs)
# plots.plot_hist_log(scs, bins=50)
# TODO save results
