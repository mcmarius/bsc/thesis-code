"""Vocabulary module"""
# import itertools
import os
import pickle

from collections import Counter
from typing import Iterator, List, Tuple

import tabulate

import constants as const
import preprocessing_stories as ps
from story import BaseStory


NR = 'Nr'


def vocab_file_name(task_name: str, dataset_type: str) -> str:
    """Vocab file name to avoid duplication"""
    os.makedirs('data/vocab/', exist_ok=True)
    if const.CUT_DATASET:
        if const.CLAMP_SOURCE_TEXT and const.CLAMP_TARGET_TEXT:
            return os.path.join('data', 'vocab',
                                '{}_{}_vocab_{}_{}.pickle'.format(task_name, dataset_type, const.MAX_EXAMPLES,
                                                                  const.SEQ_MAX_LENGTH))
        return os.path.join('data', 'vocab',
                            '{}_{}_vocab_{}.pickle'.format(task_name, dataset_type, const.MAX_EXAMPLES))
    if const.CLAMP_SOURCE_TEXT and const.CLAMP_TARGET_TEXT:
        return os.path.join('data', 'vocab',
                            '{}_{}_vocab_all_ex_{}.pickle'.format(task_name, dataset_type, const.SEQ_MAX_LENGTH))
    return os.path.join('data', 'vocab', '{}_{}_vocab.pickle'.format(task_name, dataset_type))


def clamp_text(text: List[str], max_length: int = const.SEQ_MAX_LENGTH) -> List[str]:
    """Trim input according to max_length"""
    if not const.CLAMP_SOURCE_TEXT:
        return text
    n = len(text)
    return text[0:min(n, max_length) - 1]  # -1 because of '<EOS>'


def clamp_story(story):
    return BaseStory(clamp_text(story.text),
                     [clamp_text(story.summaries[0] + story.summaries[1]
                                 if len(story.summaries) > 1
                                 else story.summaries[0])])


def build_vocab(task_name: str, dataset_type: str) -> None:
    """Build vocabulary for a certain dataset_type if it doesn't exist"""
    file_name = vocab_file_name(task_name, dataset_type)
    if not os.path.exists(file_name):
        # misleading name...
        train_stories, _ = ps.load_dataset(task_name, dataset_type)
        if const.CUT_DATASET and dataset_type == 'train':
            train_stories = train_stories[:const.MAX_EXAMPLES]
        if const.CLAMP_SOURCE_TEXT and const.CLAMP_TARGET_TEXT:
            train_stories = [clamp_story(story) for story in train_stories]
        all_words = (word for story in train_stories
                     for text in (story.text, story.summaries)
                     for sentence in text for word in sentence)
        text_words = (word for story in train_stories
                      for text in (story.text,)
                      for sentence in text for word in sentence)
        summaries_words = (word for story in train_stories
                           for text in (story.summaries,)
                           for sentence in text for word in sentence)
        all_frequencies = Counter(all_words)
        text_frequencies = Counter(text_words)
        summaries_frequencies = Counter(summaries_words)
        with open(file_name, 'wb') as file:
            pickle.dump((all_frequencies, text_frequencies, summaries_frequencies), file)


def most_common_words(task_name: str, top_n: int, dataset_type: str = 'train') -> Iterator[str]:
    """Returns top_n words for a given dataset"""
    build_vocab(task_name, dataset_type)

    with open(vocab_file_name(task_name, dataset_type), 'rb') as file:
        frequencies, _, _ = pickle.load(file)
        vocab = (word for word, _ in frequencies.most_common(top_n))
        return vocab


def return_vocab(task_name: str, dataset_type: str = 'train') -> Tuple[Counter, Counter, Counter]:
    """Returns frequencies for a given dataset"""
    build_vocab(task_name, dataset_type)

    with open(vocab_file_name(task_name, dataset_type), 'rb') as file:
        return pickle.load(file)


def count_pair(counter, count_type: str):
    if count_type == 'len':  # else 'val'
        return len(counter)
    return sum(counter.values())


def counts(task_name: str, dataset_type: str, count_type: str, n_t: int = None, n_s: int = None):
    """Returns (vocab_length, token_count) for texts, summaries and intersection"""
    _, t_f, s_f = return_vocab(task_name, dataset_type)
    c_f = t_f & s_f
    t_f, s_f, c_f = dict(t_f.most_common(n_t)), dict(s_f.most_common(n_s)), dict(c_f.most_common(n_s))
    c_p = count_pair
    return c_p(t_f, count_type), c_p(s_f, count_type), c_p(c_f, count_type)


def cut_counts(max_examples: int, task_name: str, dataset_type: str, count_type: str, n_t: int = None, n_s: int = None):
    const.CUT_DATASET = True
    const.MAX_EXAMPLES = max_examples
    return counts(task_name, dataset_type, count_type, n_t, n_s)


def all_counts(task_name: str, dataset_type: str, count_type: str, n_t: int = None, n_s: int = None):
    const.CUT_DATASET = False
    return counts(task_name, dataset_type, count_type, n_t, n_s)


def table_counts(task_name: str, count_type: str, n_t: int = None, n_s: int = None):
    # stories, _ = ps.load_dataset(task_name, 'train')
    n_stories = {'cnn': 90266, 'dm': 196961}
    max_example_counts = [2 ** x for x in range(9, 17)]  # 512..65536
    pairs = [(x, cut_counts(x, task_name, 'train', count_type, n_t, n_s)) for x in max_example_counts]
    pairs.append((n_stories[task_name], all_counts(task_name, 'train', count_type, n_t, n_s)))
    return pairs


def print_frequency_table(task_name: str, count_type: str, table_data,
                          headers, table_format: str = 'plain', i: str = '0',
                          n_t: str = None, n_s: str = None):
    table = tabulate.tabulate(table_data, headers=headers, tablefmt=table_format, floatfmt='.2f')
    print(table)
    with open(os.path.join(const.TABLES_DIR,
                           'table_{}_{}_{}_{}_{}.tex'.format(task_name, i, count_type, n_t, n_s)),
              'w') as f:
        f.write(table)


def print_full_vocab_table(task_name: str, count_type: str, table_format: str = 'plain', i: str = '0'):
    pairs = table_counts(task_name, count_type)
    if count_type == 'len':
        headers = ['{} exemple', '{} cuvinte text', '{} cuvinte rezumat', '{} cuvinte comune']
    else:
        headers = ['{} exemple', '{} token-uri text', '{} token-uri rezumat', '{} token-uri comune']
    table_data = [(x, *y) for x, y in pairs]
    headers = [x.format(NR) for x in headers]
    print_frequency_table(task_name, count_type, table_data, headers, table_format, i, '', '')


def p_str(t1, t2):
    # return '{:.3f}%'.format(100*t1/t2) if t1 < t2 else '100.00%'
    return 100 * t1 / t2 if t1 < t2 else 100


def most_common_table(task_name: str, count_type: str, n_t: int, n_s: int,
                      table_format: str = 'plain', i: str = '1'):
    full_pairs = table_counts(task_name, count_type)
    most_common_pairs = table_counts(task_name, count_type, n_t, n_s)
    if count_type == 'len':
        headers = ['{} exemple'.format(NR), '% cuvinte text', '% cuvinte rezumat', '% cuvinte comune']
    else:
        headers = ['{} exemple'.format(NR), '% token-uri text', '% token-uri rezumat', '% token-uri comune']
    table_data = [(nr, *[p_str(xt2, xt1) for xt2, xt1 in zip(t2, t1)])
                  for ((nr, t1), (nr2, t2)) in zip(full_pairs, most_common_pairs)]
    print_frequency_table(task_name, count_type, table_data, headers, table_format, i, str(n_t), str(n_s))


def train_dev(m: int, task_name: str, cut_flag: bool, n_t: int = None, n_s: int = None):
    const.CUT_DATASET = cut_flag
    const.MAX_EXAMPLES = m
    _, t_f, s_f = return_vocab(task_name, 'train')
    if n_t and n_s:
        t_f, s_f = Counter(dict(t_f.most_common(n_t))), Counter(dict(s_f.most_common(n_s)))
    return t_f, s_f


def tr_d_helper(task_name: str, n_t: int = None, n_s: int = None):
    max_example_counts = [2 ** x for x in range(9, 17)]  # 512..65536
    counters = [(x, train_dev(x, task_name, True, n_t, n_s)) for x in max_example_counts]
    n_stories = {'cnn': 90266, 'dm': 196961}
    counters.append((n_stories[task_name], train_dev(0, task_name, False, n_t, n_s)))
    _, dt_f, ds_f = return_vocab(task_name, 'dev')
    return counters, dt_f, ds_f


def tr_d1(task_name: str, count_type: str, n_t: int = None, n_s: int = None):
    _, dt_f, ds_f = tr_d_helper(task_name, n_t, n_s)
    n_stories = {'cnn': 1220, 'dm': 12148}
    c_p = count_pair
    return [(n_stories[task_name], c_p(dt_f, count_type), c_p(ds_f, count_type))]
    # return [(x[0], *c_p(dt_f, count_type), *c_p(ds_f, count_type)) for x in counters]
    # return dt_f, ds_f, counters  # tuple(itertools.chain(c_p(dt_f), c_p(ds_f), c_p(c_t), c_p(c_s)))


def tr_d2(task_name: str, count_type: str, n_t: int = None, n_s: int = None):
    counters, dt_f, ds_f = tr_d_helper(task_name, n_t, n_s)
    c_p = count_pair
    return [(x[0], c_p(dt_f - x[1][0], count_type), c_p(ds_f - x[1][1], count_type)) for x in counters]


def dev_stats(task_name: str, count_type: str, table_format: str):
    table_data = tr_d1(task_name, count_type)
    if count_type == 'len':
        headers = ['{} exemple', '{} cuvinte text', '{} cuvinte rezumat', '{} cuvinte comune']
    else:
        headers = ['{} exemple', '{} token-uri text', '{} token-uri rezumat', '{} token-uri comune']
    headers = [x.format(NR) for x in headers]
    print_frequency_table(task_name, count_type, table_data, headers, table_format, 'dev_stats')


def train_dev_diff(task_name: str, count_type: str, table_format: str, n_t: int = None, n_s: int = None):
    table_data = tr_d2(task_name, count_type, n_t, n_s)
    if count_type == 'len':
        headers = ['{} exemple', '{} cuvinte text', '{} cuvinte rezumat', '{} cuvinte comune']
    else:
        headers = ['{} exemple', '{} token-uri text', '{} token-uri rezumat', '{} token-uri comune']
    headers = [x.format(NR) for x in headers]
    print_frequency_table(task_name, count_type, table_data, headers, table_format, 'train_dev_diff',
                          str(n_t), str(n_s))
