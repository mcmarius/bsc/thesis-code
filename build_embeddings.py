"""Perform NLP computations"""
import datetime
import os
import pickle
from typing import List

import matplotlib.pyplot as plt
import pandas as pd
from adjustText import adjust_text
from gensim.models import Word2Vec
from gensim.parsing.preprocessing import preprocess_string
from nltk import sent_tokenize
from sklearn.manifold import TSNE

import constants as const
import manage_stories as mns
import manage_vocab as mnv
import preprocessing as pre


def save_sent_tokenized_stories(task_name: str):
    """Save stories in tokenized form"""

    train_stories, _ = mns.return_stories(task_name, 'train')

    texts = (preprocess_string(story.full_text(), pre.preprocessing_filters()) for story in train_stories)

    split_sentences = (sent.split() for text in texts for sent in sent_tokenize(text))

    # we pick only relevant sentences
    # long sentences are only enumerations
    sentences = [sentence for sentence in split_sentences if 2 < len(sentence) < 64]

    with open(os.path.join('sent_tokenized_cache',
                           '{}_sentence_tokenized.pickle'.format(task_name)), 'wb') as file:
        pickle.dump(sentences, file)


def check_existing_sent_cache(task_name: str, ) -> None:
    file_name = os.path.join('sent_tokenized_cache',
                             '{}_sentence_tokenized.pickle'.format(task_name))
    if not os.path.exists(file_name):
        save_sent_tokenized_stories(task_name)


def load_sent_tokenized_stories(task_name: str) -> List[str]:
    """Load tokenized stories"""

    check_existing_sent_cache(task_name)
    with open(os.path.join('sent_tokenized_cache',
                           '{}_sentence_tokenized.pickle'.format(task_name)), 'rb') as file:
        sentences = pickle.load(file)
        return sentences


def save_embedding(task_name: str, size: int = const.EMBEDDING_SIZE, vocab_size: int = const.VOCAB_SIZE):
    """Compute word2vec"""

    split_sentences = load_sent_tokenized_stories(task_name)

    model = Word2Vec(min_count=1, workers=4, size=size, max_vocab_size=vocab_size)
    model.build_vocab(split_sentences)
    model.train(split_sentences, total_examples=model.corpus_count, epochs=model.iter)
    model.save(os.path.join('embeddings', '{}_{}_v{}_word2vec.model'.format(task_name, size, vocab_size)))


def adjust_labels(data_frame: pd.DataFrame):
    """Adjust overlapping labels"""

    words = (w for w, _ in data_frame.iterrows())
    positions = (position for _, position in data_frame.iterrows())
    x_data = (x[0] for x in positions)
    y_data = (x[1] for x in positions)

    texts = []
    for _x, _y, _s in zip(x_data, y_data, words):
        texts.append(plt.text(_x, _y, _s))

    adjust_text(texts, x=y_data, y=x_data, autoalign='y',
                only_move={'points': 'y', 'text': 'y'}, force_points=0.15)


def save_plot(figure: plt.figure):
    """Saves both generated plot and raw figure"""

    current_time = "_{:%Y-%m-%d_%H:%M:%S}".format(datetime.datetime.now())
    file_name = 'figure{}.svg'.format(current_time)
    figure.savefig(os.path.join('plots', file_name), format='svg')


def load_embedding(task_name: str, size: int = const.EMBEDDING_SIZE, vocab_size: int = const.VOCAB_SIZE) -> Word2Vec:
    """Load word2vec model"""

    return Word2Vec.load(os.path.join('embeddings', '{}_{}_v{}_word2vec.model'.format(task_name, size, vocab_size)))


def plot_vec(task_name: str):
    """Plots the loaded w2v model"""

    model = load_embedding(task_name)
    vocab = mnv.most_common_words(task_name, 200)
    x_vocab = model[vocab]

    tsne = TSNE(n_components=2)
    x_tsne = tsne.fit_transform(x_vocab)
    data_frame = pd.DataFrame(x_tsne, index=vocab, columns=['x', 'y'])

    fig = plt.figure(figsize=[32, 18])
    axis = fig.add_subplot(1, 1, 1)
    axis.scatter(data_frame['x'], data_frame['y'])

    adjust_labels(data_frame)

    save_plot(fig)
