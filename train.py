"""Train model according to parameters in constants.py"""
import os
import random
from copy import deepcopy
from sys import argv
# from typing import List

# import matplotlib.pyplot as plt
import tensorboardX
import torch
import torch.nn as nn
import tqdm

import constants as const
import models
import prepare_data

import train_eval_helper as teh


device = const.device
torch.set_default_tensor_type('torch.FloatTensor')
logger = tensorboardX.SummaryWriter(log_dir=os.path.join('runs', teh.experiment_name('t')))


def train(input_tensor: torch.Tensor, target_tensor: torch.Tensor,
          # fields,
          epoch: int,
          encoder: nn.Module, decoder: nn.Module,
          encoder_optimizer: torch.optim.Optimizer,
          decoder_optimizer: torch.optim.Optimizer,
          loss_criterion: nn.Module) \
        -> float:
    encoder = encoder.cuda()
    encoder.train()
    encoder_hidden = encoder.init_hidden()
    encoder_optimizer.zero_grad()
    decoder_optimizer.zero_grad()

    # input_length = input_tensor.size(0)
    target_length = target_tensor.size(0)

    # encoder_outputs = torch.zeros(const.SEQ_MAX_LENGTH, encoder.hidden_size, device=device)
    # d_field = fields['trg']

    loss = 0

    input_tensor, lengths, target_tensor = teh.get_sequence_lengths(input_tensor, target_tensor)
    # for i in range(input_length):
    encoder_outputs, encoder_hidden = encoder(input_tensor, lengths, encoder_hidden)
    # ^ encoder_output
    # encoder_output shape is [1, 1, 128] -> we want [128]
    # encoder_outputs[i] = encoder_output[0, 0]

    decoder_input = target_tensor[:, 0]
    # num_layers, num_directions, batch_size, hidden_size
    enc_hidden = encoder_hidden.view(const.N_LAYERS, 2, const.BATCH_SIZE, -1)
    decoder_hidden = enc_hidden[:, 0, :, :] + enc_hidden[:, 1, :, :]
    decoder = decoder.cuda()
    decoder.train()

    # teacher_forcing_ratio = const.TF_R1 \
    #     if epoch < const.TF_1 else \
    #     const.TF_R2 if epoch < const.TF_2 else const.TF_E
    teacher_forcing_ratio = 0.9  # (const.EPOCHS - epoch) / const.EPOCHS
    use_teacher_forcing = True if random.random() < teacher_forcing_ratio else False

    if use_teacher_forcing:
        for i in range(1, target_length):
            if const.USE_ATTN:
                decoder_output, decoder_hidden, attn_weights, loss_ = decoder(decoder_input, decoder_hidden,
                                                                              encoder_outputs, target_tensor[:, i])
            else:
                decoder_output, decoder_hidden, loss_ = decoder(decoder_input, decoder_hidden, target_tensor[:, i])

            if not const.ADA_SOFTMAX:
                loss_ = loss_criterion(decoder_output.view(const.BATCH_SIZE, const.TARGET_VOCAB_SIZE),
                                       target_tensor[:, i])
            decoder_input = target_tensor[:, i]
            loss += loss_
    else:
        for i in range(1, target_length):
            if const.USE_ATTN:
                decoder_output, decoder_hidden, attn_weights, loss_ = decoder(decoder_input, decoder_hidden,
                                                                              encoder_outputs, target_tensor[:, i])
            else:
                decoder_output, decoder_hidden, loss_ = decoder(decoder_input, decoder_hidden, target_tensor[:, i])

            if const.ADA_SOFTMAX:
                decoder_input = decoder_output
            else:
                _, top_i = decoder_output.topk(1, dim=2)
                loss_ = loss_criterion(decoder_output.view(const.BATCH_SIZE, const.TARGET_VOCAB_SIZE),
                                       target_tensor[:, i])
                decoder_input = top_i.squeeze().detach()
            # if decoder_input.item() == d_field.numericalize([[d_field.eos_token]], device=device).item():
            #     break
            loss += loss_

    loss.backward()

    # gradient clipping
    nn.utils.clip_grad_norm_(encoder.parameters(), 1)
    nn.utils.clip_grad_norm_(decoder.parameters(), 1)

    encoder_optimizer.step(None)
    decoder_optimizer.step(None)

    return loss.item() / target_length


def requires_grad(p: nn.Parameter) -> bool:
    return p.requires_grad


def train_epochs(task_name: str, encoder: nn.Module, decoder: nn.Module,
                 train_ds: prepare_data.SummarizationDataset,
                 val_ds: prepare_data.SummarizationDataset,
                 epochs: int = const.EPOCHS,
                 # print_interval: int = const.PRINT_INTERVAL,
                 eval_interval: int = const.EVAL_INTERVAL,
                 save_interval: int = const.SAVE_INTERVAL) \
        -> None:
    plot_losses = []
    plot_val_losses = []

    encoder_optimizer = torch.optim.RMSprop(encoder.parameters(), lr=const.LEARNING_RATE, centered=True)
    decoder_optimizer = torch.optim.RMSprop(decoder.parameters(), lr=const.LEARNING_RATE, centered=True)

    train_it, val_it, _ = teh.get_iterators(train_ds, val_ds)
    # _, dummy_val_it, _ = teh.get_iterators(train_ds, val_ds)

    # nn_input, nn_output = next(iter(dummy_val_it))
    # nn_input, lengths, _ = teh.get_sequence_lengths(nn_input, nn_output)

    loss_criterion = nn.NLLLoss(ignore_index=1)

    start_epoch, loss = 0, 10
    if len(argv) > 2:
        if argv[1] == "resume":
            encoder, decoder, encoder_optimizer, decoder_optimizer, start_epoch, loss \
                = teh.load_state(encoder, decoder, encoder_optimizer, decoder_optimizer, int(argv[2]), task_name)

    j = start_epoch
    for epoch in range(start_epoch, epochs):
        print_total_loss = 0
        plot_total_loss = 0

        o_encoder, o_decoder, o_encoder_optimizer, o_decoder_optimizer, o_loss \
            = deepcopy(encoder), deepcopy(decoder), deepcopy(encoder_optimizer), deepcopy(decoder_optimizer), loss

        try:
            for i, input_tensor, target_tensor in tqdm.tqdm(train_it):
                if input_tensor.size(1) != const.BATCH_SIZE:
                    # skip last mini-batch
                    continue
                input_tensor.cuda()
                target_tensor.cuda()
                loss = train(input_tensor, target_tensor, epoch, encoder, decoder,
                             encoder_optimizer, decoder_optimizer, loss_criterion)
                logger.add_scalar('individual_train_loss', loss, epoch * len(train_it) + i)
                print_total_loss += loss
                plot_total_loss += loss

        except KeyboardInterrupt:
            teh.save_state(o_encoder, o_decoder,
                           o_encoder_optimizer, o_decoder_optimizer, epoch - 1, o_loss, task_name)
            # show_plot(plot_losses, plot_val_losses)
            # logger.add_graph(encoder, [nn_input, lengths], verbose=True)
            return
        # if epoch % print_interval == 0:
        print('\n', print_total_loss / len(train_it), epoch)

        if epoch % eval_interval == 0:
            val_loss = teh.evaluate_randomly(encoder, decoder, train_ds, val_it, loss_criterion, j, logger)
            j += 1
            print('\n', val_loss / len(val_it), '\n')
            plot_val_losses.append(val_loss / len(val_it))
            logger.add_scalar('validation_loss', val_loss / len(val_it), epoch)
        plot_losses.append(plot_total_loss / len(train_it))
        logger.add_scalar('train_loss', plot_total_loss / len(train_it), epoch)
        # memory is in GB
        logger.add_scalar('memory_allocated', round(torch.cuda.memory_allocated(0)/1024**3, 2), epoch)
        logger.add_scalar('memory_cached', round(torch.cuda.memory_cached(0)/1024**3, 2), epoch)

        if epoch % save_interval == 0:
            teh.save_state(encoder, decoder, encoder_optimizer, decoder_optimizer, epoch, loss, task_name)
    teh.save_state(encoder, decoder, encoder_optimizer, decoder_optimizer, epochs, loss, task_name)
    # logger.add_graph(encoder, nn_input, verbose=True)
    # show_plot(plot_losses, plot_val_losses)


# def show_plot(points: List, val_points: list) -> None:
#    plt.plot(range(0, const.EPOCHS), points)
#    plt.plot(range(0, const.EPOCHS, const.EVAL_INTERVAL), val_points)
#    plt.ylabel('Loss')
#    plt.xlabel('Epochs')
#    plt.show()


def run():
    task_name = const.TASK_NAME
    train_ds, val_ds, _ = teh.load_dataset_wrappers(task_name)

    print('Finished loading data')
    encoder, decoder = models.get_model()
    train_epochs(task_name, encoder, decoder, train_ds, val_ds)


if __name__ == "__main__":
    run()
    logger.close()
