"""More preprocessing and caching"""
import itertools
import os
import pickle
from typing import List, Tuple

from gensim.parsing.preprocessing import preprocess_string
from nltk import sent_tokenize

import manage_stories as mns
import preprocessing as pre
from story import BaseStory, Story


def preprocess_story(story: Story, remove_stopwords: bool = True) -> BaseStory:
    """Preprocess the contents of a story using gensim's preprocess_string

    preprocess_string also performs word tokenization"""

    if remove_stopwords:
        filters = pre.text_filters()
    else:
        filters = pre.preprocessing_filters()
    text = [preprocess_string(sentence, filters) for sentence in sent_tokenize(story.text)]

    filters = pre.summary_filters()
    summaries = [preprocess_string(summary, filters) for summary in story.summaries]

    return BaseStory(text, summaries)


def save_raw_aux(file_name, stories: List[Story]) -> None:
    """Save preprocessed stories to filename"""
    stories = [preprocess_story(story) for story in stories]
    with open(file_name, 'wb') as file:
        pickle.dump(stories, file)


def load_dataset(task_name: str, dataset_type: str) \
        -> Tuple[List[BaseStory], List[BaseStory]]:
    """Load tokenized stories"""

    os.makedirs('data/sent_tokenized_cache/', exist_ok=True)
    file_name = os.path.join('data', 'sent_tokenized_cache',
                             '{}_sent_tokenized_{}_stories.pickle'.format(task_name, dataset_type))
    if not os.path.exists(file_name):
        stories = mns.return_stories(task_name, dataset_type)
        save_raw_aux(file_name, stories)
    with open(file_name, 'rb') as file:
        stories = pickle.load(file)
        w_stories = [BaseStory(list(itertools.chain.from_iterable([[*sent, '.'] for sent in story.text])),
                               story.summaries)
                     for story in stories]
        return stories, w_stories
